package com.onscreen;

import android.test.suitebuilder.annotation.SmallTest;

import junit.framework.TestCase;

/**
 * Created by srinivas on 27/1/16.
 */
public class UnitTest extends TestCase {
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @SmallTest
    public void testcheckAddtion() {
        int results = Utils.addNumbers(2, 3);
        assertEquals("Success", 5, results);

    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
