package com.onscreen;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by srinivas on 1/2/16.
 */
public class Rest {

    public void practice(String name) {
        RestCalls.uploadPics service = ServiceGenerator.createService(RestCalls.uploadPics.class);
        service.upload(name, new Callback<Object>() {

            @Override
            public void success(Object o, Response response) {

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
}
