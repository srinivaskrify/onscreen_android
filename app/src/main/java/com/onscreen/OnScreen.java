package com.onscreen;

import android.app.Application;

import com.facebook.stetho.Stetho;

/**
 * Created by srinivas on 1/2/16.
 */
public class OnScreen extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }
}
