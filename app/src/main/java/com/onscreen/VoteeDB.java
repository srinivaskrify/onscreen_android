package com.onscreen;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by srinivas on 19/12/15.
 */
public class VoteeDB {
    Context con;
    SQLiteDatabase db;
    Cursor cr;
    DB_STORE help;

    public VoteeDB(Context con1) {
        // TODO Auto-generated construct or stub
        this.con = con1;
        help = new DB_STORE(con, DB_PARAMS.DATABASE_NAME, null,
                DB_PARAMS.DATABASE_VERSION);
        db = help.getReadableDatabase();
    }

    public class DB_STORE extends SQLiteOpenHelper {

        public DB_STORE(Context context, String name, SQLiteDatabase.CursorFactory factory,
                        int version) {
            super(context, DB_PARAMS.DATABASE_NAME, null,
                    DB_PARAMS.DATABASE_VERSION);
            // TODO Auto-generated constructor stub
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // TODO Auto-generated method stub
            db.execSQL(DB_QUERIES.CREATE_TABLE.Friend_Details);
            db.execSQL(DB_QUERIES.CREATE_TABLE.BlockedUser_Details);
            db.execSQL(DB_QUERIES.CREATE_TABLE.Questions_Details);
            db.execSQL(DB_QUERIES.CREATE_TABLE.Answer_Details);
            db.execSQL(DB_QUERIES.CREATE_TABLE.Inbox_Details);
            db.execSQL(DB_QUERIES.CREATE_TABLE.Notifications);
            System.out
                    .println("************************* TableCreated ****************************************");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // TODO Auto-generated method stub
			/*
			 * db.execSQL("DROP TABLE IF EXISTS "+DB_QUERIES.CREATE_TABLE.Chat_BG
			 * ); onCreate(db);
			 */
        }

    }

    public void FriendsInsert(String email, String facebook_id,
                              String twitter_id, String twitter_screen_name, String profile_pic,
                              String first_name, String last_name, String date_of_birth,
                              String gender, String phone, String phone_verified,
                              String email_verified, String location, String bio,
                              String language, String date, String number_of_questions,
                              String number_of_votes, String unique_id, String type, String id,
                              String name) {

        SQLiteDatabase db = help.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DB_PARAMS.COLOUMNS.friendstable.email, email);
        values.put(DB_PARAMS.COLOUMNS.friendstable.facebook_id, facebook_id);
        values.put(DB_PARAMS.COLOUMNS.friendstable.twitter_id, twitter_id);
        values.put(DB_PARAMS.COLOUMNS.friendstable.twitter_screen_name,
                twitter_screen_name);
        values.put(DB_PARAMS.COLOUMNS.friendstable.profile_pic, profile_pic);
        values.put(DB_PARAMS.COLOUMNS.friendstable.first_name, first_name);
        values.put(DB_PARAMS.COLOUMNS.friendstable.last_name, last_name);
        values.put(DB_PARAMS.COLOUMNS.friendstable.date_of_birth, date_of_birth);
        values.put(DB_PARAMS.COLOUMNS.friendstable.gender, gender);
        values.put(DB_PARAMS.COLOUMNS.friendstable.phone, phone);
        values.put(DB_PARAMS.COLOUMNS.friendstable.phone_verified,
                phone_verified);
        values.put(DB_PARAMS.COLOUMNS.friendstable.email_verified,
                email_verified);
        values.put(DB_PARAMS.COLOUMNS.friendstable.location, location);
        values.put(DB_PARAMS.COLOUMNS.friendstable.bio, bio);
        values.put(DB_PARAMS.COLOUMNS.friendstable.language, language);
        values.put(DB_PARAMS.COLOUMNS.friendstable.date, date);
        values.put(DB_PARAMS.COLOUMNS.friendstable.number_of_questions,
                number_of_questions);
        values.put(DB_PARAMS.COLOUMNS.friendstable.number_of_votes,
                number_of_votes);
        values.put(DB_PARAMS.COLOUMNS.friendstable.unique_id, unique_id);
        values.put(DB_PARAMS.COLOUMNS.friendstable.type, type);
        values.put(DB_PARAMS.COLOUMNS.friendstable.id, id);
        values.put(DB_PARAMS.COLOUMNS.friendstable.name, name);
        db.insert(DB_PARAMS.TABLE.FRIENDS_DETAILS, null, values);

        db.close();
    }

    public void BlockedUsersInsert(String email, String facebook_id,
                                   String twitter_id, String twitter_screen_name, String profile_pic,
                                   String first_name, String last_name, String date_of_birth,
                                   String gender, String phone, String phone_verified,
                                   String email_verified, String location, String bio,
                                   String language, String date, String number_of_questions,
                                   String number_of_votes, String unique_id, String type) {

        SQLiteDatabase db = help.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DB_PARAMS.COLOUMNS.blockeduserstable.email, email);
        values.put(DB_PARAMS.COLOUMNS.blockeduserstable.facebook_id,
                facebook_id);
        values.put(DB_PARAMS.COLOUMNS.blockeduserstable.twitter_id, twitter_id);
        values.put(DB_PARAMS.COLOUMNS.blockeduserstable.twitter_screen_name,
                twitter_screen_name);
        values.put(DB_PARAMS.COLOUMNS.blockeduserstable.profile_pic,
                profile_pic);
        values.put(DB_PARAMS.COLOUMNS.blockeduserstable.first_name, first_name);
        values.put(DB_PARAMS.COLOUMNS.blockeduserstable.last_name, last_name);
        values.put(DB_PARAMS.COLOUMNS.blockeduserstable.date_of_birth,
                date_of_birth);
        values.put(DB_PARAMS.COLOUMNS.blockeduserstable.gender, gender);
        values.put(DB_PARAMS.COLOUMNS.blockeduserstable.phone, phone);
        values.put(DB_PARAMS.COLOUMNS.blockeduserstable.phone_verified,
                phone_verified);
        values.put(DB_PARAMS.COLOUMNS.blockeduserstable.email_verified,
                email_verified);
        values.put(DB_PARAMS.COLOUMNS.blockeduserstable.location, location);
        values.put(DB_PARAMS.COLOUMNS.blockeduserstable.bio, bio);
        values.put(DB_PARAMS.COLOUMNS.blockeduserstable.language, language);
        values.put(DB_PARAMS.COLOUMNS.blockeduserstable.date, date);
        values.put(DB_PARAMS.COLOUMNS.blockeduserstable.number_of_questions,
                number_of_questions);
        values.put(DB_PARAMS.COLOUMNS.blockeduserstable.number_of_votes,
                number_of_votes);
        values.put(DB_PARAMS.COLOUMNS.blockeduserstable.unique_id, unique_id);

        values.put(DB_PARAMS.COLOUMNS.friendstable.type, type);
        db.insert(DB_PARAMS.TABLE.BLOCKEDUSER_DETAILS, null, values);

        db.close();
    }

    public void InsertQuestionData(String questionid, String title,
                                   String locationlat, String locationlong, String enablecomments,
                                   String layouttype, String is_public, String token,
                                   String created_at, String updated_at, String yes_id,
                                   String yes_text, String yes_file, String yes_votes,
                                   String yes_voted, String no_id, String no_text, String no_file,
                                   String no_votes, String no_voted, String partial_discussions,
                                   String total_number_of_invitations,
                                   String total_number_of_discussions) {

        SQLiteDatabase db = help.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DB_PARAMS.COLOUMNS.QuestionsTable.questionid, questionid);
        values.put(DB_PARAMS.COLOUMNS.QuestionsTable.title, title);
        values.put(DB_PARAMS.COLOUMNS.QuestionsTable.locationlat, locationlat);
        values.put(DB_PARAMS.COLOUMNS.QuestionsTable.locationlong, locationlong);
        values.put(DB_PARAMS.COLOUMNS.QuestionsTable.enablecomments,
                enablecomments);
        values.put(DB_PARAMS.COLOUMNS.QuestionsTable.layouttype, layouttype);
        values.put(DB_PARAMS.COLOUMNS.QuestionsTable.is_public, is_public);
        values.put(DB_PARAMS.COLOUMNS.QuestionsTable.token, token);
        values.put(DB_PARAMS.COLOUMNS.QuestionsTable.created_at, created_at);
        values.put(DB_PARAMS.COLOUMNS.QuestionsTable.updated_at, updated_at);
        values.put(DB_PARAMS.COLOUMNS.QuestionsTable.yes_id, yes_id);
        values.put(DB_PARAMS.COLOUMNS.QuestionsTable.yes_text, yes_text);
        values.put(DB_PARAMS.COLOUMNS.QuestionsTable.yes_file, yes_file);
        values.put(DB_PARAMS.COLOUMNS.QuestionsTable.yes_votes, yes_votes);
        values.put(DB_PARAMS.COLOUMNS.QuestionsTable.yes_voted, yes_voted);
        values.put(DB_PARAMS.COLOUMNS.QuestionsTable.no_id, no_id);
        values.put(DB_PARAMS.COLOUMNS.QuestionsTable.no_text, no_text);
        values.put(DB_PARAMS.COLOUMNS.QuestionsTable.no_file, no_file);
        values.put(DB_PARAMS.COLOUMNS.QuestionsTable.no_votes, no_votes);

        values.put(DB_PARAMS.COLOUMNS.QuestionsTable.no_voted, no_voted);
        values.put(DB_PARAMS.COLOUMNS.QuestionsTable.partial_discussions,
                partial_discussions);
        values.put(
                DB_PARAMS.COLOUMNS.QuestionsTable.total_number_of_invitations,
                total_number_of_invitations);
        values.put(
                DB_PARAMS.COLOUMNS.QuestionsTable.total_number_of_discussions,
                total_number_of_discussions);
        db.insert(DB_PARAMS.TABLE.QUESTIONDETAILS, null, values);

        db.close();
    }

    public void InsertAnswersData(String questionid, String title,
                                  String enablecomments, String layouttype, String is_public,
                                  String token, String created_at, String updated_at, String brand,
                                  String category, String yes_id, String yes_text, String yes_file,
                                  String yes_votes, String yes_voted, String no_id, String no_text,
                                  String no_file, String no_votes, String no_voted,
                                  String partial_discussions, String total_number_of_invitations,
                                  String total_number_of_discussions, String type, String creator) {

        SQLiteDatabase db = help.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DB_PARAMS.COLOUMNS.AnswersTable.questionid, questionid);
        values.put(DB_PARAMS.COLOUMNS.AnswersTable.title, title);
        values.put(DB_PARAMS.COLOUMNS.AnswersTable.enablecomments,
                enablecomments);
        values.put(DB_PARAMS.COLOUMNS.AnswersTable.layouttype, layouttype);
        values.put(DB_PARAMS.COLOUMNS.AnswersTable.is_public, is_public);
        values.put(DB_PARAMS.COLOUMNS.AnswersTable.token, token);
        values.put(DB_PARAMS.COLOUMNS.AnswersTable.created_at, created_at);
        values.put(DB_PARAMS.COLOUMNS.AnswersTable.updated_at, updated_at);
        values.put(DB_PARAMS.COLOUMNS.AnswersTable.brand, brand);
        values.put(DB_PARAMS.COLOUMNS.AnswersTable.category, category);
        values.put(DB_PARAMS.COLOUMNS.AnswersTable.yes_id, yes_id);
        values.put(DB_PARAMS.COLOUMNS.AnswersTable.yes_text, yes_text);
        values.put(DB_PARAMS.COLOUMNS.AnswersTable.yes_file, yes_file);
        values.put(DB_PARAMS.COLOUMNS.AnswersTable.yes_votes, yes_votes);
        values.put(DB_PARAMS.COLOUMNS.AnswersTable.yes_voted, yes_voted);
        values.put(DB_PARAMS.COLOUMNS.AnswersTable.no_id, no_id);
        values.put(DB_PARAMS.COLOUMNS.AnswersTable.no_text, no_text);
        values.put(DB_PARAMS.COLOUMNS.AnswersTable.no_file, no_file);
        values.put(DB_PARAMS.COLOUMNS.AnswersTable.no_votes, no_votes);

        values.put(DB_PARAMS.COLOUMNS.AnswersTable.no_voted, no_voted);
        values.put(DB_PARAMS.COLOUMNS.AnswersTable.partial_discussions,
                partial_discussions);
        values.put(DB_PARAMS.COLOUMNS.AnswersTable.total_number_of_invitations,
                total_number_of_invitations);
        values.put(DB_PARAMS.COLOUMNS.AnswersTable.total_number_of_discussions,
                total_number_of_discussions);
        values.put(DB_PARAMS.COLOUMNS.AnswersTable.type, type);
        values.put(DB_PARAMS.COLOUMNS.AnswersTable.creator, creator);
        db.insert(DB_PARAMS.TABLE.ANSWERDETAILS, null, values);

        db.close();
    }

    public void InsertInboxData(String questionid, String title,
                                String enablecomments, String layouttype, String is_public,
                                String token, String created_at, String updated_at, String brand,
                                String creator, String yes_id, String yes_text, String yes_file,
                                String yes_votes, String yes_voted, String no_id, String no_text,
                                String no_file, String no_votes, String no_voted,
                                String partial_discussions, String total_number_of_invitations,
                                String total_number_of_discussions, String type, String dummy) {

        SQLiteDatabase db = help.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DB_PARAMS.COLOUMNS.InboxTable.questionid, questionid);
        values.put(DB_PARAMS.COLOUMNS.InboxTable.title, title);
        values.put(DB_PARAMS.COLOUMNS.InboxTable.enablecomments, enablecomments);
        values.put(DB_PARAMS.COLOUMNS.InboxTable.layouttype, layouttype);
        values.put(DB_PARAMS.COLOUMNS.InboxTable.is_public, is_public);
        values.put(DB_PARAMS.COLOUMNS.InboxTable.token, token);
        values.put(DB_PARAMS.COLOUMNS.InboxTable.created_at, created_at);
        values.put(DB_PARAMS.COLOUMNS.InboxTable.updated_at, updated_at);
        values.put(DB_PARAMS.COLOUMNS.InboxTable.brand, brand);
        values.put(DB_PARAMS.COLOUMNS.InboxTable.creator, creator);
        values.put(DB_PARAMS.COLOUMNS.InboxTable.yes_id, yes_id);
        values.put(DB_PARAMS.COLOUMNS.InboxTable.yes_text, yes_text);
        values.put(DB_PARAMS.COLOUMNS.InboxTable.yes_file, yes_file);
        values.put(DB_PARAMS.COLOUMNS.InboxTable.yes_votes, yes_votes);
        values.put(DB_PARAMS.COLOUMNS.InboxTable.yes_voted, yes_voted);
        values.put(DB_PARAMS.COLOUMNS.InboxTable.no_id, no_id);
        values.put(DB_PARAMS.COLOUMNS.InboxTable.no_text, no_text);
        values.put(DB_PARAMS.COLOUMNS.InboxTable.no_file, no_file);
        values.put(DB_PARAMS.COLOUMNS.InboxTable.no_votes, no_votes);

        values.put(DB_PARAMS.COLOUMNS.InboxTable.no_voted, no_voted);
        values.put(DB_PARAMS.COLOUMNS.InboxTable.partial_discussions,
                partial_discussions);
        values.put(DB_PARAMS.COLOUMNS.InboxTable.total_number_of_invitations,
                total_number_of_invitations);
        values.put(DB_PARAMS.COLOUMNS.InboxTable.total_number_of_discussions,
                total_number_of_discussions);
        values.put(DB_PARAMS.COLOUMNS.InboxTable.type, type);
        values.put(DB_PARAMS.COLOUMNS.InboxTable.tagged_users, dummy);
        db.insert(DB_PARAMS.TABLE.INBOXDETAILS, null, values);

        db.close();
    }

    public void InsertNotifications(String i, String message,
                                    String collapse_key, String a, String m, String from,
                                    String status, String jsonobj, String dummy1, String dummy2) {

        SQLiteDatabase db = help.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DB_PARAMS.COLOUMNS.Notifications.i, i);
        values.put(DB_PARAMS.COLOUMNS.Notifications.message, message);
        values.put(DB_PARAMS.COLOUMNS.Notifications.collapse_key, collapse_key);
        values.put(DB_PARAMS.COLOUMNS.Notifications.a, a);
        values.put(DB_PARAMS.COLOUMNS.Notifications.m, m);
        values.put(DB_PARAMS.COLOUMNS.Notifications.from, from);
        values.put(DB_PARAMS.COLOUMNS.Notifications.status, status);
        values.put(DB_PARAMS.COLOUMNS.Notifications.jsonobj, jsonobj);
        values.put(DB_PARAMS.COLOUMNS.Notifications.dummy1, dummy1);
        values.put(DB_PARAMS.COLOUMNS.Notifications.dummy2, dummy2);
        db.insert(DB_PARAMS.TABLE.NOTIFICATIONS, null, values);

        db.close();
    }

    public Cursor getFriendsDetails() {
        Cursor c = null;
        try {

            SQLiteDatabase db = help.getReadableDatabase();

            c = db.rawQuery("SELECT * FROM " + DB_PARAMS.TABLE.FRIENDS_DETAILS,
                    null);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return c;

    }

    public Cursor getBlockedUserDetails() {
        Cursor c = null;
        try {

            SQLiteDatabase db = help.getReadableDatabase();

            c = db.rawQuery("SELECT * FROM "
                    + DB_PARAMS.TABLE.BLOCKEDUSER_DETAILS, null);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return c;

    }

    public Cursor getQuestionsDetails() {
        Cursor c = null;
        try {

            SQLiteDatabase db = help.getReadableDatabase();

            c = db.rawQuery("SELECT * FROM " + DB_PARAMS.TABLE.QUESTIONDETAILS,
                    null);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return c;

    }

    public Cursor getAnswersDetails(String type) {
        Cursor c = null;
        try {

            SQLiteDatabase db = help.getReadableDatabase();

            c = db.rawQuery("SELECT * FROM " + DB_PARAMS.TABLE.ANSWERDETAILS
                    + " WHERE " + DB_PARAMS.COLOUMNS.AnswersTable.type + " = '"
                    + type + "'", null);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return c;

    }

    public Cursor getInboxDetails(String type) {
        Cursor c = null;
        try {

            SQLiteDatabase db = help.getReadableDatabase();

            c = db.rawQuery("SELECT * FROM " + DB_PARAMS.TABLE.INBOXDETAILS
                    + " WHERE " + DB_PARAMS.COLOUMNS.InboxTable.type + " = '"
                    + type + "'", null);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return c;

    }

    public Cursor getNotificationsData() {
        Cursor c = null;
        try {

            SQLiteDatabase db = help.getReadableDatabase();

            c = db.rawQuery("SELECT * FROM " + DB_PARAMS.TABLE.NOTIFICATIONS,
                    null);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return c;

    }

    public void ClearInboxTable() {
        Log.i("ClearInboxTable", "ClearInboxTable");
        SQLiteDatabase db = help.getWritableDatabase();
        db.execSQL("delete from " + DB_PARAMS.TABLE.INBOXDETAILS);
        db.close();
    }

    public void ClearNotificationsTable() {
        Log.i("ClearInboxTable", "ClearInboxTable");
        SQLiteDatabase db = help.getWritableDatabase();
        db.execSQL("delete from " + DB_PARAMS.TABLE.NOTIFICATIONS);
        db.close();
    }

    public void ClearQuetionsTable() {
        Log.i("ClearQuetionsTable", "ClearQuetionsTable");
        SQLiteDatabase db = help.getWritableDatabase();
        db.execSQL("delete from " + DB_PARAMS.TABLE.QUESTIONDETAILS);
        db.close();
    }

    public void ClearAllTables() {
        SQLiteDatabase db = help.getWritableDatabase();
        db.execSQL("delete from " + DB_PARAMS.TABLE.INBOXDETAILS);
        db.execSQL("delete from " + DB_PARAMS.TABLE.QUESTIONDETAILS);
        db.execSQL("delete from " + DB_PARAMS.TABLE.ANSWERDETAILS);
        db.execSQL("delete from " + DB_PARAMS.TABLE.FRIENDS_DETAILS);
        db.execSQL("delete from " + DB_PARAMS.TABLE.BLOCKEDUSER_DETAILS);
        db.execSQL("delete from " + DB_PARAMS.TABLE.NOTIFICATIONS);
        db.close();
    }

    // public void ClearAnswersTable() {
    // Log.i("ClearAnswersTable", "ClearAnswersTable");
    // SQLiteDatabase db = help.getWritableDatabase();
    // db.execSQL("delete from " + DB_PARAMS.TABLE.ANSWERDETAILS);
    // db.close();
    // }
    public void ClearAnswersTable(String type) {
        Log.i("ClearAnswersTable", "ClearAnswersTable");
        SQLiteDatabase db = help.getWritableDatabase();
        db.execSQL("delete from " + DB_PARAMS.TABLE.ANSWERDETAILS + " WHERE "
                + DB_PARAMS.COLOUMNS.AnswersTable.type + " = '" + type + "'");

        db.close();
    }

    public void ClearBlockedUser() {
        Log.i("ClearBlockedUser", "ClearBlockedUser");
        SQLiteDatabase db = help.getWritableDatabase();
        db.execSQL("delete from " + DB_PARAMS.TABLE.BLOCKEDUSER_DETAILS);
        db.close();
    }

    public void ClearFriendsList() {
        Log.i("ClearFriendsList", "ClearFriendsList");
        SQLiteDatabase db = help.getWritableDatabase();
        db.execSQL("delete from " + DB_PARAMS.TABLE.FRIENDS_DETAILS);
        db.close();
    }

    public void updateNotificationStatusVal(String status, String i) {
        ContentValues values = new ContentValues();
        SQLiteDatabase db = help.getWritableDatabase();
        values.put(DB_PARAMS.COLOUMNS.Notifications.status, status);
        db.update(DB_PARAMS.TABLE.NOTIFICATIONS, values,
                DB_PARAMS.COLOUMNS.Notifications.i + "='" + i + "'", null);
        db.close();
    }


    public void updateNotificationQuestionVoteStatus(String status, String i) {
        ContentValues values = new ContentValues();
        SQLiteDatabase db = help.getWritableDatabase();
        values.put(DB_PARAMS.COLOUMNS.Notifications.dummy2, status);
        db.update(DB_PARAMS.TABLE.NOTIFICATIONS, values,
                DB_PARAMS.COLOUMNS.Notifications.i + "='" + i + "'", null);
        db.close();
    }

    public void updateNotificationFriendsRequestStatus(String status, String i) {
        ContentValues values = new ContentValues();
        SQLiteDatabase db = help.getWritableDatabase();
        values.put(DB_PARAMS.COLOUMNS.Notifications.dummy1, status);
        db.update(DB_PARAMS.TABLE.NOTIFICATIONS, values,
                DB_PARAMS.COLOUMNS.Notifications.i + "='" + i + "'", null);
        db.close();
    }

    public void UpdateAllNotificationStatus(String status) {
        SQLiteDatabase db = help.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DB_PARAMS.COLOUMNS.Notifications.status, status);
        db.update(DB_PARAMS.TABLE.NOTIFICATIONS, values, null, null);
        db.close();
    }
}