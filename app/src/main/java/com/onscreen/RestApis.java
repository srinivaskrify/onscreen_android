package com.onscreen;

/**
 * Created by srinivas on 21/9/15.
 */
public class RestApis {

    public static String StagingUrl = "http://staging.krify.com/horee/v1";
    public static String StagingUrl2 = "http://staging.krify.com/horee/v2";
    public static String BaseURL = StagingUrl2;

    private static String getSignUp = "/signup";
    private static String getUploadingpic = "/picuploading";
    public  static String getlogin = "/login";
    private static String getupdateprofile = "/updateprofile";
    private static String getforgotpassword = "/forgotpassword";
    private static String getsearchcontacts = "/searchcontacts";
    private static String getaddcontacts = "/addcontact";
    private static String getlogout = "/logout";
    private static String getacceptrequests = "/acceptrequests";
    private static String getcontacts = "/getcontacts";
    private static String getactivityfeed = "/activityfeed";
    private static String getcreategroup = "/creategroup";
    private static String getgroupstouserid = "/getgroupstouserid";
    private static String searchgroups = "/searchgroups";
    private static String editgroup = "/editgroup";
    private static String getgp_details = "/gp_details";
    private static String addmemberstogroup = "/addmemberstogroup";
    private static String sendgroup_request = "/sendgroup_request";
    private static String acceptgrouprequests = "/acceptgrouprequests";
    private static String exitmemberfromgroup = "/exitmemberfromgroup";
    private static String addpost = "/addpost";
    private static String addpostlike = "/addpostlike";
    private static String addpostcomment = "/addpostcomment";
    private static String getgroupchat = "/getgroupchat";
    private static String getpostlikes = "/getpostlikes";
    private static String getpostcomments = "/getpostcomments";
    private static String editcomment = "/editcomment";
    private static String clearchat = "/clearchat";
    private static String removememberfromgroup = "/removememberfromgroup";
    private static String deletechat = "/deletechat";
    private static String viewcount = "/viewcount";
    private static String getuserchat = "/getuserchat";
    private static String updatesettings = "/updatesettings";
    private static String suggestions = "/suggestions";
    private static String createeventorpoll = "/createeventorpoll";
    private static String jobs = "/jobs";
    private static String searchjobs = "/searchjobs";
    private static String getuserpollorevents = "/getuserpollorevents";
    private static String answeringpoll = "/answeringpoll";
    private static String getvotedusers = "/getvotedusers";
    private static String mypollresponses = "/mypollresponses";
    private static String createthread = "/createthread";
    private static String sendthreadchat = "/sendthreadchat";
    private static String changethreadstatus = "/changethreadstatus";
    private static String jobcategories = "/jobcategories";
    private static String deleteactivityfeed = "/deleteactivityfeed";
    private static String clearthreadchat = "/clearthreadchat";
    private static String getsubcat2cat = "/getsubcat2cat";
    private static String skills = "/skills";
    private static String skillsandcurrencies = "/skillsandcurrencies";
    private static String createjob = "/createjob";
    private static String sharejob = "/sharejob";
    private static String userthreads = "/userthreads";
    private static String userthreadchat = "/userthreadchat";
    private static String myjobs = "/myjobs";
    private static String sendjobchat = "/sendjobchat";
    private static String viewuserjobchat = "/viewuserjobchat";
    private static String clearjobchat = "/clearjobchat";
    private static String opportunities = "/opportunities";
    private static String deletethread= "/deletethread";







    public static String signUp = BaseURL + getSignUp;
    public static String uploadingpic = BaseURL + getUploadingpic;
    public static String login = BaseURL + getlogin;
    public static String updateprofile = BaseURL + getupdateprofile;
    public static String forgotpassword = BaseURL + getforgotpassword;
    public static String searchcontacts = BaseURL + getsearchcontacts;
    public static String addcontacts = BaseURL + getaddcontacts;
    public static String logout = BaseURL + getlogout;
    public static String acceptrequests = BaseURL + getacceptrequests;
    public static String getusercontacts = BaseURL + getcontacts;
    public static String activityfeed = BaseURL + getactivityfeed;
    public static String creategroup = BaseURL + getcreategroup;
    public static String getgroups = BaseURL + getgroupstouserid;
    public static String searchgroupresults = BaseURL + searchgroups;
    public static String editgroupApi = BaseURL + editgroup;
    public static String gp_details = BaseURL + getgp_details;
    public static String addmemberstogroupApi = BaseURL + addmemberstogroup;
    public static String sendgroup_requestApi = BaseURL + sendgroup_request;
    public static String acceptgrouprequestsApi = BaseURL + acceptgrouprequests;
    public static String exitmemberfromgroupApi = BaseURL + exitmemberfromgroup;
    public static String addpostApi = BaseURL + addpost;
    public static String addpostlikeApi = BaseURL + addpostlike;
    public static String addpostcommentApi = BaseURL + addpostcomment;
    public static String getgroupchatApi = BaseURL + getgroupchat;
    public static String getpostlikesApi = BaseURL + getpostlikes;
    public static String getpostcommentsApi = BaseURL + getpostcomments;
    public static String editcommentApi = BaseURL + editcomment;
    public static String clearchatApi = BaseURL + clearchat;
    public static String removememberfromgroupApi = BaseURL + removememberfromgroup;
    public static String deletechatApi = BaseURL + deletechat;
    public static String viewcountApi = BaseURL + viewcount;
    public static String getuserchatApi = BaseURL + getuserchat;
    public static String updatesettingsApi = BaseURL + updatesettings;
    public static String suggestionsApi = BaseURL + suggestions;
    public static String createeventorpollApi = BaseURL + createeventorpoll;
    public static String jobsApi = BaseURL + jobs;
    public static String searchjobsApi = BaseURL + searchjobs;
    public static String getuserpolloreventsApi = BaseURL + getuserpollorevents;
    public static String answeringpollApi= BaseURL + answeringpoll;
    public static String getvotedusersApi= BaseURL + getvotedusers;
    public static String mypollresponsesApi= BaseURL + mypollresponses;
    public static String createathreadApi= BaseURL + createthread;
    public static String sendthreadchatApi= BaseURL + sendthreadchat;
    public static String changethreadstatusApi= BaseURL + changethreadstatus;
    public static String jobcategoriesApi= BaseURL + jobcategories;
    public static String deleteactivityfeedApi= BaseURL + deleteactivityfeed;
    public static String clearthreadchatApi= BaseURL + clearthreadchat;
    public static String getsubcat2catApi= BaseURL + getsubcat2cat;
    public static String skillsApi= BaseURL + skills;
    public static String skillsandcurrenciesApi= BaseURL + skillsandcurrencies;
    public static String createjobApi= BaseURL + createjob;
    public static String sharejobApi= BaseURL + sharejob;
    public static String userthreadsApi= BaseURL + userthreads;
    public static String userthreadchatApi= BaseURL + userthreadchat;
    public static String myjobsApi= BaseURL + myjobs;
    public static String sendjobchatApi= BaseURL + sendjobchat;
    public static String viewuserjobchatApi= BaseURL + viewuserjobchat;
    public static String clearjobchatApi= BaseURL + clearjobchat;
    public static String opportunitiesApi= BaseURL + opportunities;
    public static String deleteThreadApi= BaseURL + deletethread;

}
