package com.onscreen;

import android.app.Activity;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by srinivas on 22/9/15.
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();
    Activity activity;

    public ViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return IntroFragment.newInstance(Color.parseColor("#F8A429"), position); // blue
            default:
                return IntroFragment.newInstance(Color.parseColor("#F8A429"), position); // green
        }
//        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Activity activity, Fragment fragment, String title) {
        mFragmentList.add(fragment);
        this.activity = activity;
        mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = mFragmentTitleList.get(position);
        return title;


    }


}
