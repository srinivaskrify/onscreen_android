package com.onscreen;

public class DB_PARAMS {

	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "Votee_testing";
	public static final String vartype = "TEXT";
	public static final String INTEGER = "INTEGER";
	public static final String PRIMARY = "PRIMARY";
	public static final String KEY = "KEY";
	public static final String AUTOINC = "AUTOINCREMENT";
	public static final String DropTable = "DROP TABLE IF EXISTS";
	public static String select = "SELECT";
	public static String from = "FROM";
	public static String where = "WHERE";
	public static String and = "AND";
	public static String selectAll = "SELECT  * FROM ";
	public static String id = "id";
	public static String CREATE_TABLE = "CREATE TABLE ";
	public static String QVartype = " TEXT";
	public static String QAutoinc = " INTEGER PRIMARY KEY AUTOINCREMENT";
	public static String orderby = " order by ";
	public static String desc = " desc";

	// public static String thirty_limit = " LIMIT 0 , 30";

	public interface TABLE {

		String USER = "USER_TABLE";
		String FRIENDS_DETAILS = "FRIENDS_DETAILS";
		String BLOCKEDUSER_DETAILS = "BLOCKEDUSER_DETAILS";
		String QUESTIONDETAILS = "QUESTIONDETAILS";
		String ANSWERDETAILS = "ANSWERDETAILS";
		String INBOXDETAILS = "INBOXDETAILS";
		String SAMPLE="SAMPLE";
		String NOTIFICATIONS="NOTIFICATIONS";
	}

	public static class COLOUMNS {

		public interface usertable {

			String ID = "user_id";
			String NAME = "name";
			String user_i20 = "user_i20";
			String PHONE = "phone";
			String user_status_msg = "user_status_msg";
			String IMAGE = "image";
			String CHANNEL = "channel";

		}

		public interface friendstable {

			String email = "email";
			String facebook_id = "facebook_id";
			String twitter_id = "twitter_id";
			String twitter_screen_name = "twitter_screen_name";
			String profile_pic = "profile_pic";
			String first_name = "first_name";
			String last_name = "last_name";
			String date_of_birth = "date_of_birth";
			String gender = "gender";
			String phone = "phone";
			String phone_verified = "phone_verified";
			String email_verified = "email_verified";
			String location = "location";
			String bio = "bio";
			String language = "language";
			String date = "date";
			String number_of_questions = "number_of_questions";
			String number_of_votes = "number_of_votes";
			String unique_id = "unique_id";
			String type = "type";
			String id = "id";
			String name = "name";
		}

		public interface blockeduserstable {

			String email = "email";
			String facebook_id = "facebook_id";
			String twitter_id = "twitter_id";
			String twitter_screen_name = "twitter_screen_name";
			String profile_pic = "profile_pic";
			String first_name = "first_name";
			String last_name = "last_name";
			String date_of_birth = "date_of_birth";
			String gender = "gender";
			String phone = "phone";
			String phone_verified = "phone_verified";
			String email_verified = "email_verified";
			String location = "location";
			String bio = "bio";
			String language = "language";
			String date = "date";
			String number_of_questions = "number_of_questions";
			String number_of_votes = "number_of_votes";
			String unique_id = "unique_id";
			String type = "type";

		}

		public interface QuestionsTable {
			String questionid = "questionid";
			String title = "title";
			String locationlat = "locationlat";
			String locationlong = "locationlong";
			String enablecomments = "enablecomments";
			String layouttype = "layouttype";
			String is_public = "is_public";
			String token = "token";
			String created_at = "created_at";
			String updated_at = "updated_at";
			String yes_id = "yes_id";
			String yes_text = "yes_text";
			String yes_file = "yes_file";
			String yes_votes = "yes_votes";
			String yes_voted = "yes_voted";
			String no_id = "no_id";
			String no_text = "no_text";
			String no_file = "no_file";
			String no_votes = "no_votes";
			String no_voted = "no_voted";
			String partial_discussions = "partial_discussions";
			String total_number_of_invitations = "total_number_of_invitations";
			String total_number_of_discussions = "total_number_of_discussions";
		}

		public interface AnswersTable {
			String questionid = "questionid";
			String title = "title";
			String enablecomments = "enablecomments";
			String layouttype = "layouttype";
			String is_public = "is_public";
			String token = "token";
			String created_at = "created_at";
			String updated_at = "updated_at";
			String brand = "brand";
			String category = "category";
			String yes_id = "yes_id";
			String yes_text = "yes_text";
			String yes_file = "yes_file";
			String yes_votes = "yes_votes";
			String yes_voted = "yes_voted";
			String no_id = "no_id";
			String no_text = "no_text";
			String no_file = "no_file";
			String no_votes = "no_votes";
			String no_voted = "no_voted";
			String partial_discussions = "partial_discussions";
			String total_number_of_invitations = "total_number_of_invitations";
			String total_number_of_discussions = "total_number_of_discussions";
			String type = "type";
			String creator = "creator";
		}

		public interface InboxTable {
			String questionid = "questionid";
			String title = "title";
			String enablecomments = "enablecomments";
			String layouttype = "layouttype";
			String is_public = "is_public";
			String token = "token";
			String created_at = "created_at";
			String updated_at = "updated_at";
			String brand = "brand";
			String creator = "creator";
			String yes_id = "yes_id";
			String yes_text = "yes_text";
			String yes_file = "yes_file";
			String yes_votes = "yes_votes";
			String yes_voted = "yes_voted";
			String no_id = "no_id";
			String no_text = "no_text";
			String no_file = "no_file";
			String no_votes = "no_votes";
			String no_voted = "no_voted";
			String partial_discussions = "partial_discussions";
			String total_number_of_invitations = "total_number_of_invitations";
			String total_number_of_discussions = "total_number_of_discussions";
			String type = "type";
			String tagged_users = "tagged_users";
		}

		public interface Notifications {
			String i = "i";
			String message = "message";
			String collapse_key = "collapse_key";
			String a = "a";
			String m = "m";
			String from = "from1";
			String status = "status";
			String jsonobj = "jsonobj";
			String dummy1="dummy1";
			String dummy2="dummy2";
		}



		public interface Sample{
			String id="id";
			String name="name";
			String msg="msg";
			String time="time";
			String timeStamp="timestamp";

		}

	}
}
