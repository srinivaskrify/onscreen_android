package com.onscreen;

import android.graphics.drawable.AnimationDrawable;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

/**
 * Created by srinivas on 18/2/16.
 */
public class IntroPageTransformer implements ViewPager.PageTransformer {

    @Override
    public void transformPage(View page, float position) {

        // Get the page index from the tag. This makes
        // it possible to know which page index you're
        // currently transforming - and that can be used
        // to make some important performance improvements.
        int pagePosition = (int) page.getTag();

        // Here you can do all kinds of stuff, like get the
        // width of the page and perform calculations based
        // on how far the user has swiped the page.
        int pageWidth = page.getWidth();
        float pageWidthTimesPosition = pageWidth * position;
        float absPosition = Math.abs(position);

        // Now it's time for the effects
        if (position <= -1.0f || position >= 1.0f) {

            // The page is not visible. This is a good place to stop
            // any potential work / animations you may have running.

        } else if (position == 0.0f) {

            // The page is selected. This is a good time to reset Views
            // after animations as you can't always count on the PageTransformer
            // callbacks to match up perfectly.

            if (TextUtils.equals(page.getTag().toString(), "1")) {
                showThinkChoice(page);
            }
            if (TextUtils.equals(page.getTag().toString(), "2")) {
                animateContacts(page);
            }
            if (TextUtils.equals(page.getTag().toString(), "3")) {
                showBulb(page);
            }


        } else {

            // The page is currently being scrolled / swiped. This is
            // a good place to show animations that react to the user's
            // swiping as it provides a good user experience.

            // Let's start by animating the title.
            // We want it to fade as it scrolls out
            if (TextUtils.equals(page.getTag().toString(), "1")) {


          /*      View cloud1 = page.findViewById(R.id.cloud1);
                View cloud2 = page.findViewById(R.id.cloud2);
                View musicstand = page.findViewById(R.id.music_stand);
                View music_note = page.findViewById(R.id.music_note);


*/


                // Now the description. We also want this one to
                // fade, but the animation should also slowly move
                // down and out of the screen
       /*     View description = page.findViewById(R.id.description);
            description.setTranslationY(-pageWidthTimesPosition / 2f);
            description.setAlpha(1.0f - absPosition);
*/
                Log.d("pageWidthTimesPosition", "" + pageWidthTimesPosition);


                // Now, we want the image to move to the right,
                // i.e. in the opposite direction of the rest of the
                // content while fading out
//            View computer = page.findViewById(R.id.computer);

                // We're attempting to create an effect for a View
                // specific to one of the pages in our ViewPager.
                // In other words, we need to check that we're on
                // the correct page and that the View in question
                // isn't null.
        /*    if (pagePosition == 0 && computer != null) {
                computer.setAlpha(1.0f - absPosition);
                computer.setTranslationX(-pageWidthTimesPosition * 1.5f);
            }*/

                // Finally, it can be useful to know the direction
                // of the user's swipe - if we're entering or exiting.
                // This is quite simple:
                if (position < 0) {
                    // Create your out animation here
              /*      cloud1.setTranslationX(pageWidthTimesPosition / 1f);
                    cloud2.setTranslationX(pageWidthTimesPosition / 1f);
                    music_note.setAlpha(1.0f - absPosition);
                    musicstand.setAlpha(1.0f - absPosition);
                    music_note.setTranslationX(pageWidthTimesPosition * 1.5f);
                    musicstand.setTranslationX(pageWidthTimesPosition * 1.5f);*/
                    takeAway(page, pageWidthTimesPosition);
                    hideThinkChoice(page);


                } else {
                    // Create your in animation here

//                    cloud2.setTranslationY(-pageWidthTimesPosition / 2f);
//                    music_note.setAlpha(1.0f - absPosition);

//                    musicstand.setAlpha(1.0f - absPosition);
//                    musicstand.setTranslationX(pageWidthTimesPosition * 1f);
                    showGirlandAB(page, pageWidthTimesPosition);
                }
            } else if (TextUtils.equals(page.getTag().toString(), "2")) {
                if (position < 0) {
                    // Create your out animation here
                } else {
                    // Create your in animation here
                    showContacts(page, pageWidthTimesPosition);
                }


            } else if (TextUtils.equals(page.getTag().toString(), "3")) {

                if (position < 0) {
                    // Create your out animation here
                    hideDecissionAnim(page, pageWidthTimesPosition);
                } else {
                    // Create your in animation here
                    showDecissionAnim(page, pageWidthTimesPosition);
                }


            }
        }
    }


    public void showThinkChoice(View page) {
        View left = page.findViewById(R.id.left);
        View center = page.findViewById(R.id.center);
        View right = page.findViewById(R.id.right);
        left.setVisibility(View.VISIBLE);
        center.setVisibility(View.VISIBLE);
        right.setVisibility(View.VISIBLE);
        Animation show15 = AnimationUtils.loadAnimation(page.getContext(), R.anim.toptobottom1500);
        Animation show25 = AnimationUtils.loadAnimation(page.getContext(), R.anim.toptobottom2500);
        Animation show35 = AnimationUtils.loadAnimation(page.getContext(), R.anim.toptobottom3500);
        left.startAnimation(show15);
        center.startAnimation(show25);
        right.startAnimation(show35);
    }

    public void hideThinkChoice(View page) {
        View left = page.findViewById(R.id.left);
        View center = page.findViewById(R.id.center);
        View right = page.findViewById(R.id.right);
        left.setVisibility(View.INVISIBLE);
        center.setVisibility(View.INVISIBLE);
        right.setVisibility(View.INVISIBLE);
    }


    public void showGirlandAB(View page, float pageWidthTimesPosition) {
        View aorb = page.findViewById(R.id.aorb);
        View girl = page.findViewById(R.id.girl);
        aorb.setTranslationY(-pageWidthTimesPosition / 1f);
        girl.setTranslationX(pageWidthTimesPosition * 1.5f);
    }

    public void takeAway(View page, float pageWidthTimesPosition) {
        View aorb = page.findViewById(R.id.aorb);
        View girl = page.findViewById(R.id.girl);
        aorb.setTranslationX(pageWidthTimesPosition * 1f);
        girl.setTranslationX(pageWidthTimesPosition * 1f);
    }


    public void showContacts(View page, float pageWidthTimesPosition) {
        View image1 = page.findViewById(R.id.image1);
        View image2 = page.findViewById(R.id.image2);
        View image3 = page.findViewById(R.id.image3);
        View image4 = page.findViewById(R.id.image4);

        image1.setTranslationX(pageWidthTimesPosition * 1f);
        image2.setTranslationX(pageWidthTimesPosition * 1.3f);
        image3.setTranslationX(pageWidthTimesPosition * 1.5f);
        image4.setTranslationX(pageWidthTimesPosition * 1.7f);


    }


    public void animateContacts(View page) {
        View image1 = page.findViewById(R.id.image1);
        View image2 = page.findViewById(R.id.image2);
        View image3 = page.findViewById(R.id.image3);
        View image4 = page.findViewById(R.id.image4);


        ((AnimationDrawable) image1.getBackground()).start();
        ((AnimationDrawable) image3.getBackground()).start();
        ((AnimationDrawable) image4.getBackground()).start();

    }

    public void showDecissionAnim(View page, float pageWidthTimesPosition) {
        View vote = page.findViewById(R.id.vote);
        View smile = page.findViewById(R.id.smile);
        View idea = page.findViewById(R.id.idea);
        vote.setTranslationY(-pageWidthTimesPosition / 1f);
        smile.setTranslationY(pageWidthTimesPosition * 1f);
    }

    public void hideDecissionAnim(View page, float pageWidthTimesPosition) {
        View vote = page.findViewById(R.id.vote);
        View smile = page.findViewById(R.id.smile);
        View idea = page.findViewById(R.id.idea);
        vote.setTranslationX(pageWidthTimesPosition / 1f);
        smile.setTranslationX(pageWidthTimesPosition * 1f);
        idea.setTranslationX(pageWidthTimesPosition * 1f);
    }

    public void showBulb(View page) {

        View idea = page.findViewById(R.id.idea);
        idea.setVisibility(View.VISIBLE);
    }


}