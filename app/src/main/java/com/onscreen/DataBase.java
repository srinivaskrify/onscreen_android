package com.onscreen;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by srinivas on 19/12/15.
 */
public class DataBase {

    Context context;
    SQLiteDatabase db;
    Cursor cr;
    MyDB help;


    public DataBase(Context context) {
        this.context = context;
        help = new MyDB(context, DB_PARAMS.DATABASE_NAME, null,
                DB_PARAMS.DATABASE_VERSION);
        db = help.getReadableDatabase();

    }


    public class MyDB extends SQLiteOpenHelper {

        public MyDB(Context context, String name,
                    SQLiteDatabase.CursorFactory factory, int version) {
            super(context, DB_PARAMS.DATABASE_NAME, null,
                    DB_PARAMS.DATABASE_VERSION);

        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {

            if (sqLiteDatabase != null)
                sqLiteDatabase.execSQL(sampleTable());
            else
                Toast.makeText(context, "NOT CREATED", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        }

        public String sampleTable() {
            return "create table IF NOT EXISTS "
                    + DB_PARAMS.TABLE.SAMPLE + "(_id INTEGER PRIMARY KEY ,"
                    + DB_PARAMS.COLOUMNS.Sample.id + " TEXT,"
                    + DB_PARAMS.COLOUMNS.Sample.name + " TEXT,"
                    + DB_PARAMS.COLOUMNS.Sample.msg + " TEXT,"
                    + DB_PARAMS.COLOUMNS.Sample.timeStamp + " TEXT,"
                    + DB_PARAMS.COLOUMNS.Sample.time + " TEXT)";
        }
    }


    public void insertData(String id, String name, String msg, String time, String timestamp) {
        SQLiteDatabase database = help.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DB_PARAMS.COLOUMNS.Sample.id, id);
        values.put(DB_PARAMS.COLOUMNS.Sample.name, name);
        values.put(DB_PARAMS.COLOUMNS.Sample.msg, msg);
        values.put(DB_PARAMS.COLOUMNS.Sample.time, time);
        values.put(DB_PARAMS.COLOUMNS.Sample.timeStamp, timestamp);

        database.insert(DB_PARAMS.TABLE.SAMPLE, null, values);
        Log.d("insertedSuccess ", "GOOD");
    }


    public List<String> getUserNames(String time) {
        List<String> usernames = new ArrayList<>();
        Cursor cursor = null;
        SQLiteDatabase database = help.getReadableDatabase();

        cursor = database.query(false, DB_PARAMS.TABLE.SAMPLE,
                new String[]{DB_PARAMS.COLOUMNS.Sample.name},
                DB_PARAMS.COLOUMNS.Sample.time + "=?",
                new String[]{time}, null, null, null, null);


        if (cursor.moveToFirst()) {
            do {
                usernames.add(cursor.getString(cursor.getColumnIndex(DB_PARAMS.COLOUMNS.Sample.name)));
            }
            while (cursor.moveToNext());

        }
        return usernames;
    }


    public void updateUserNames(String time) {
        SQLiteDatabase database = help.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DB_PARAMS.COLOUMNS.Sample.name, "JOE");

        database.update(DB_PARAMS.TABLE.SAMPLE, values, DB_PARAMS.COLOUMNS.Sample.time + "=?", new String[]{time});


    }

    public void DeleteUserNames(String time) {
        SQLiteDatabase database = help.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DB_PARAMS.COLOUMNS.Sample.name, "JOE");

        database.delete(DB_PARAMS.TABLE.SAMPLE, DB_PARAMS.COLOUMNS.Sample.time + "=?", new String[]{time});


    }

    public List<String> getLikeUserNames(String query) {
        List<String> usernames = new ArrayList<>();
        Cursor cursor = null;
        SQLiteDatabase database = help.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DB_PARAMS.COLOUMNS.Sample.name, "JOE");

        cursor = database.query(false, DB_PARAMS.TABLE.SAMPLE, new String[]{DB_PARAMS.COLOUMNS.Sample.name},
                DB_PARAMS.COLOUMNS.Sample.name + " LIKE ?", new String[]{query+"%"}, null, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                usernames.add(cursor.getString(cursor.getColumnIndex(DB_PARAMS.COLOUMNS.Sample.name)));
            }
            while (cursor.moveToNext());

        }
        return usernames;
    }

}
