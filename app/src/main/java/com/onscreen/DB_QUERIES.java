package com.onscreen;

public class DB_QUERIES {

	public interface CREATE_TABLE {

		// String USER_TABLE = "create table " + DB_PARAMS.TABLE.USER
		// + "(_id INTEGER PRIMARY KEY ,"
		// + DB_PARAMS.COLOUMNS.usertable.ID + " TEXT,"
		// + DB_PARAMS.COLOUMNS.usertable.NAME + " TEXT ,"
		//
		// + DB_PARAMS.COLOUMNS.usertable.PHONE + " TEXT,"
		// + DB_PARAMS.COLOUMNS.usertable.IMAGE + " TEXT ,"
		// + DB_PARAMS.COLOUMNS.usertable.user_status_msg + "  TEXT,"
		// + DB_PARAMS.COLOUMNS.usertable.CHANNEL + "TEXT)";

		String Friend_Details = "create table "
				+ DB_PARAMS.TABLE.FRIENDS_DETAILS + "(_id INTEGER ,"
				+ DB_PARAMS.COLOUMNS.friendstable.email + " TEXT,"
				+ DB_PARAMS.COLOUMNS.friendstable.facebook_id + " TEXT, "
				+ DB_PARAMS.COLOUMNS.friendstable.twitter_id + " TEXT ,"
				+ DB_PARAMS.COLOUMNS.friendstable.twitter_screen_name
				+ " TEXT ," + DB_PARAMS.COLOUMNS.friendstable.profile_pic
				+ " TEXT," + DB_PARAMS.COLOUMNS.friendstable.first_name
				+ " TEXT ," + DB_PARAMS.COLOUMNS.friendstable.last_name
				+ " TEXT ," + DB_PARAMS.COLOUMNS.friendstable.date_of_birth
				+ " TEXT," + DB_PARAMS.COLOUMNS.friendstable.gender + " TEXT,"
				+ DB_PARAMS.COLOUMNS.friendstable.phone + " TEXT,"
				+ DB_PARAMS.COLOUMNS.friendstable.phone_verified + " TEXT,"
				+ DB_PARAMS.COLOUMNS.friendstable.email_verified + " TEXT,"
				+ DB_PARAMS.COLOUMNS.friendstable.location + " TEXT,"
				+ DB_PARAMS.COLOUMNS.friendstable.bio + " TEXT,"
				+ DB_PARAMS.COLOUMNS.friendstable.language + " TEXT,"
				+ DB_PARAMS.COLOUMNS.friendstable.date + " TEXT,"
				+ DB_PARAMS.COLOUMNS.friendstable.number_of_questions
				+ " TEXT," + DB_PARAMS.COLOUMNS.friendstable.number_of_votes
				+ " TEXT," + DB_PARAMS.COLOUMNS.friendstable.unique_id
				+ " TEXT," + DB_PARAMS.COLOUMNS.friendstable.type + " TEXT,"
				+ DB_PARAMS.COLOUMNS.friendstable.id + " TEXT,"
				+ DB_PARAMS.COLOUMNS.friendstable.name + " TEXT)";

		String BlockedUser_Details = "create table "
				+ DB_PARAMS.TABLE.BLOCKEDUSER_DETAILS + "(_id INTEGER ,"
				+ DB_PARAMS.COLOUMNS.blockeduserstable.email + " TEXT,"
				+ DB_PARAMS.COLOUMNS.blockeduserstable.facebook_id + " TEXT, "
				+ DB_PARAMS.COLOUMNS.blockeduserstable.twitter_id + " TEXT ,"
				+ DB_PARAMS.COLOUMNS.blockeduserstable.twitter_screen_name
				+ " TEXT ," + DB_PARAMS.COLOUMNS.blockeduserstable.profile_pic
				+ " TEXT," + DB_PARAMS.COLOUMNS.blockeduserstable.first_name
				+ " TEXT ," + DB_PARAMS.COLOUMNS.blockeduserstable.last_name
				+ " TEXT ,"
				+ DB_PARAMS.COLOUMNS.blockeduserstable.date_of_birth + " TEXT,"
				+ DB_PARAMS.COLOUMNS.blockeduserstable.gender + " TEXT,"
				+ DB_PARAMS.COLOUMNS.blockeduserstable.phone + " TEXT,"
				+ DB_PARAMS.COLOUMNS.blockeduserstable.phone_verified
				+ " TEXT,"
				+ DB_PARAMS.COLOUMNS.blockeduserstable.email_verified
				+ " TEXT," + DB_PARAMS.COLOUMNS.blockeduserstable.location
				+ " TEXT," + DB_PARAMS.COLOUMNS.blockeduserstable.bio
				+ " TEXT," + DB_PARAMS.COLOUMNS.blockeduserstable.language
				+ " TEXT," + DB_PARAMS.COLOUMNS.blockeduserstable.date
				+ " TEXT,"
				+ DB_PARAMS.COLOUMNS.blockeduserstable.number_of_questions
				+ " TEXT,"
				+ DB_PARAMS.COLOUMNS.blockeduserstable.number_of_votes
				+ " TEXT," + DB_PARAMS.COLOUMNS.blockeduserstable.unique_id
				+ " TEXT," + DB_PARAMS.COLOUMNS.friendstable.type + " TEXT)";

		String Questions_Details = "create table "
				+ DB_PARAMS.TABLE.QUESTIONDETAILS + "(_id INTEGER ,"
				+ DB_PARAMS.COLOUMNS.QuestionsTable.questionid + " TEXT,"
				+ DB_PARAMS.COLOUMNS.QuestionsTable.title + " TEXT, "
				+ DB_PARAMS.COLOUMNS.QuestionsTable.locationlat + " TEXT ,"
				+ DB_PARAMS.COLOUMNS.QuestionsTable.locationlong + " TEXT ,"
				+ DB_PARAMS.COLOUMNS.QuestionsTable.enablecomments + " TEXT,"
				+ DB_PARAMS.COLOUMNS.QuestionsTable.layouttype + " TEXT ,"
				+ DB_PARAMS.COLOUMNS.QuestionsTable.is_public + " TEXT ,"
				+ DB_PARAMS.COLOUMNS.QuestionsTable.token + " TEXT,"
				+ DB_PARAMS.COLOUMNS.QuestionsTable.created_at + " TEXT,"
				+ DB_PARAMS.COLOUMNS.QuestionsTable.updated_at + " TEXT,"
				+ DB_PARAMS.COLOUMNS.QuestionsTable.yes_id + " TEXT,"
				+ DB_PARAMS.COLOUMNS.QuestionsTable.yes_text + " TEXT,"
				+ DB_PARAMS.COLOUMNS.QuestionsTable.yes_file + " TEXT,"
				+ DB_PARAMS.COLOUMNS.QuestionsTable.yes_votes + " TEXT,"
				+ DB_PARAMS.COLOUMNS.QuestionsTable.yes_voted + " TEXT,"
				+ DB_PARAMS.COLOUMNS.QuestionsTable.no_id + " TEXT,"
				+ DB_PARAMS.COLOUMNS.QuestionsTable.no_text + " TEXT,"
				+ DB_PARAMS.COLOUMNS.QuestionsTable.no_file + " TEXT,"
				+ DB_PARAMS.COLOUMNS.QuestionsTable.no_votes + " TEXT,"
				+ DB_PARAMS.COLOUMNS.QuestionsTable.no_voted + " TEXT,"

				+ DB_PARAMS.COLOUMNS.QuestionsTable.partial_discussions
				+ " TEXT,"
				+ DB_PARAMS.COLOUMNS.QuestionsTable.total_number_of_invitations
				+ " TEXT,"
				+ DB_PARAMS.COLOUMNS.QuestionsTable.total_number_of_discussions
				+ " TEXT)";

		String Answer_Details = "create table " + DB_PARAMS.TABLE.ANSWERDETAILS
				+ "(_id INTEGER ," + DB_PARAMS.COLOUMNS.AnswersTable.questionid
				+ " TEXT," + DB_PARAMS.COLOUMNS.AnswersTable.title + " TEXT, "
				+ DB_PARAMS.COLOUMNS.AnswersTable.enablecomments + " TEXT,"
				+ DB_PARAMS.COLOUMNS.AnswersTable.layouttype + " TEXT ,"
				+ DB_PARAMS.COLOUMNS.AnswersTable.is_public + " TEXT ,"
				+ DB_PARAMS.COLOUMNS.AnswersTable.token + " TEXT,"
				+ DB_PARAMS.COLOUMNS.AnswersTable.created_at + " TEXT,"
				+ DB_PARAMS.COLOUMNS.AnswersTable.updated_at + " TEXT,"
				+ DB_PARAMS.COLOUMNS.AnswersTable.brand + " TEXT,"
				+ DB_PARAMS.COLOUMNS.AnswersTable.category + " TEXT,"
				+ DB_PARAMS.COLOUMNS.AnswersTable.yes_id + " TEXT,"
				+ DB_PARAMS.COLOUMNS.AnswersTable.yes_text + " TEXT,"
				+ DB_PARAMS.COLOUMNS.AnswersTable.yes_file + " TEXT,"
				+ DB_PARAMS.COLOUMNS.AnswersTable.yes_votes + " TEXT,"
				+ DB_PARAMS.COLOUMNS.AnswersTable.yes_voted + " TEXT,"
				+ DB_PARAMS.COLOUMNS.AnswersTable.no_id + " TEXT,"
				+ DB_PARAMS.COLOUMNS.AnswersTable.no_text + " TEXT,"
				+ DB_PARAMS.COLOUMNS.AnswersTable.no_file + " TEXT,"
				+ DB_PARAMS.COLOUMNS.AnswersTable.no_votes + " TEXT,"
				+ DB_PARAMS.COLOUMNS.AnswersTable.no_voted + " TEXT,"

				+ DB_PARAMS.COLOUMNS.AnswersTable.partial_discussions
				+ " TEXT,"
				+ DB_PARAMS.COLOUMNS.AnswersTable.total_number_of_invitations
				+ " TEXT,"
				+ DB_PARAMS.COLOUMNS.AnswersTable.total_number_of_discussions
				+ " TEXT," + DB_PARAMS.COLOUMNS.AnswersTable.type + " TEXT,"
				+ DB_PARAMS.COLOUMNS.AnswersTable.creator + " TEXT)";

		String Inbox_Details = "create table " + DB_PARAMS.TABLE.INBOXDETAILS
				+ "(_id INTEGER ," + DB_PARAMS.COLOUMNS.InboxTable.questionid
				+ " TEXT," + DB_PARAMS.COLOUMNS.InboxTable.title + " TEXT, "
				+ DB_PARAMS.COLOUMNS.InboxTable.enablecomments + " TEXT,"
				+ DB_PARAMS.COLOUMNS.InboxTable.layouttype + " TEXT ,"
				+ DB_PARAMS.COLOUMNS.InboxTable.is_public + " TEXT ,"
				+ DB_PARAMS.COLOUMNS.InboxTable.token + " TEXT,"
				+ DB_PARAMS.COLOUMNS.InboxTable.created_at + " TEXT,"
				+ DB_PARAMS.COLOUMNS.InboxTable.updated_at + " TEXT,"
				+ DB_PARAMS.COLOUMNS.InboxTable.brand + " TEXT,"
				+ DB_PARAMS.COLOUMNS.InboxTable.creator + " TEXT,"
				+ DB_PARAMS.COLOUMNS.InboxTable.yes_id + " TEXT,"
				+ DB_PARAMS.COLOUMNS.InboxTable.yes_text + " TEXT,"
				+ DB_PARAMS.COLOUMNS.InboxTable.yes_file + " TEXT,"
				+ DB_PARAMS.COLOUMNS.InboxTable.yes_votes + " TEXT,"
				+ DB_PARAMS.COLOUMNS.InboxTable.yes_voted + " TEXT,"
				+ DB_PARAMS.COLOUMNS.InboxTable.no_id + " TEXT,"
				+ DB_PARAMS.COLOUMNS.InboxTable.no_text + " TEXT,"
				+ DB_PARAMS.COLOUMNS.InboxTable.no_file + " TEXT,"
				+ DB_PARAMS.COLOUMNS.InboxTable.no_votes + " TEXT,"
				+ DB_PARAMS.COLOUMNS.InboxTable.no_voted + " TEXT,"

				+ DB_PARAMS.COLOUMNS.InboxTable.partial_discussions + " TEXT,"
				+ DB_PARAMS.COLOUMNS.InboxTable.total_number_of_invitations
				+ " TEXT,"
				+ DB_PARAMS.COLOUMNS.InboxTable.total_number_of_discussions
				+ " TEXT," + DB_PARAMS.COLOUMNS.InboxTable.type + " TEXT,"
				+ DB_PARAMS.COLOUMNS.InboxTable.tagged_users + " TEXT)";

		String Notifications = "create table IF NOT EXISTS "
				+ DB_PARAMS.TABLE.NOTIFICATIONS + "(_id INTEGER PRIMARY KEY ,"
				+ DB_PARAMS.COLOUMNS.Notifications.i + " TEXT,"
				+ DB_PARAMS.COLOUMNS.Notifications.message + " TEXT,"
				+ DB_PARAMS.COLOUMNS.Notifications.collapse_key + " TEXT,"
				+ DB_PARAMS.COLOUMNS.Notifications.a + " TEXT,"
				+ DB_PARAMS.COLOUMNS.Notifications.m + " TEXT,"
				+ DB_PARAMS.COLOUMNS.Notifications.from + " TEXT,"
				+ DB_PARAMS.COLOUMNS.Notifications.status + " TEXT,"
				+ DB_PARAMS.COLOUMNS.Notifications.jsonobj + " TEXT,"
				+ DB_PARAMS.COLOUMNS.Notifications.dummy1 + " TEXT,"
				+ DB_PARAMS.COLOUMNS.Notifications.dummy2 + " TEXT)";

	}

}