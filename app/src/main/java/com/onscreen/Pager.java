package com.onscreen;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Pager extends AppCompatActivity {
    TabLayout tabLayout;
    ViewPager viewpager;
    ViewPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pager);
        init();
    }


    public void init() {
//        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewpager);

//        tabLayout.setupWithViewPager(viewpager);
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(this, new Fragment1(),"fragment1");
        adapter.addFragment(this, new Fragment2(),"fragment2");
        adapter.addFragment(this, new Fragment1(),"fragment3");
        adapter.addFragment(this, new Fragment1(),"fragment4");
        adapter.addFragment(this, new Fragment1(),"fragment5");
        viewPager.setAdapter(adapter);
        viewPager.setPageTransformer(false, new IntroPageTransformer());

    }
}
